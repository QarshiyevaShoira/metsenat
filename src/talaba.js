export default [
    {
      id: 1,
      name: 'Alimov Abror Xabibullayevich',
      tur: 'Bakalavr',
      otm: 'Toshkent shahridagi INHA Universiteti',
      price: '14 000 000',
      kontrakt: '30 000 000'
    },
    {
      id: 2,
      name: 'Saimov Rustam Saimjonovich',
      tur: 'Magistr',
      otm: 'O’zbekiston milliy universiteti',
      price: '28 000 000',
      kontrakt: '28 000 000'
    },
    {
      id: 3,
      name: 'Sanginov Otabek Muratovich',
      tur: 'Magistr',
      otm: 'Toshkent davlat texnika universiteti',
      price: '0',
      kontrakt: '24 000 000'
    },
    {
      id: 4,
      name: 'Nazarov Sanjar Olimovich',
      tur: 'Bakalavr',
      otm: 'Toshkent davlat iqtisodiyot universiteti',
      price: '20 000 000',
      kontrakt: '25 000 000'
    },
    {
      id: 5,
      name: 'Alimov Abror Xabibullayevich',
      tur: 'Bakalavr',
      otm: 'Toshkent shahridagi INHA Universiteti',
      price: '14 000 000',
      kontrakt: '30 000 000'
    },
    {
      id: 6,
      name: 'Saimov Rustam Saimjonovich',
      tur: 'Magistr',
      otm: 'O’zbekiston milliy universiteti',
      price: '28 000 000',
      kontrakt: '28 000 000'
    },
    {
      id: 7,
      name: 'Sanginov Otabek Muratovich',
      tur: 'Magistr',
      otm: 'Toshkent davlat texnika universiteti',
      price: '0',
      kontrakt: '24 000 000'
    },
    {
      id: 8,
      name: 'Nazarov Sanjar Olimovich',
      tur: 'Bakalavr',
      otm: 'Toshkent davlat iqtisodiyot universiteti',
      price: '20 000 000',
      kontrakt: '25 000 000'
    },
  ]