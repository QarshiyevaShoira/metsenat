import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'petition',
    component: () => import( '@/views/PetitionView')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import( '@/views/LoginView')
  },
  {
    path: '/home',
    name: 'home',
    component: () => import( '@/views/HomeView')
  },
  {
    path: '/sponsors',
    name: 'sponsors',
    component: () => import( '@/views/SponsorsView')
  },
  {
    path: '/sponsor-eye/:sponsorId',
    name: 'sponsor-eye',
    component: () => import( '@/views/SponsorsView/SponsorEyeView/SponsorEyeView.vue')
  },
  {
    path: '/students',
    name: 'students',
    component: () => import( '@/views/StudentsView')
  },
  {
    path: '/students-eye/:id',
    name: 'students-eye',
    component: () => import('@/views/StudentsView/StudentsEye/StudentsEye.vue')
  },
  {
    path: '/student-add',
    name: 'student-add',
    component: () => import('@/views/StudentsView/StudentAddView/StudentAddView.vue')
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
